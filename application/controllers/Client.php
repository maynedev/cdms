<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 5/26/16
 * Time: 6:37 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller
{
    function index($client_code)
    {
        $client = $this->crud->read("clients", array("client_code" => $client_code));
        if (count($client) == 0) {
            redirect('nowhere');
        }
        $var['client'] = (object)$client[0];
        $var['members'] = $this->crud->read("users", array("deleted" => false, "client_id" => $var['client']->client_id));
        $var['user_categories'] = $this->crud->read("user_categories", array("deleted" => false, "client_id" => $var['client']->client_id));
        $var['page'] = array("Client profile", $var['client']->client_name);
        $this->load->template('client_profile', $var);
    }

    function _remap($param)
    {
        if ($param == "register_member")
            $this->register_member();
        else if ($param == "register_category")
            $this->register_category();
        else
            $this->index($param);
    }

    function register_category()
    {
        $client = $this->crud->read("clients", array("client_id" => $_POST['client_id']));
        $client_code = $client[0]->client_code;
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');
        $this->form_validation->set_rules('category_description', 'Category Description', 'required');
        if ($this->form_validation->run() == false) {
            $this->index($client[0]->client_code);
        } else {

            $data = array(
                "client_id" => $_POST['client_id'],
                "category_name" => $_POST['category_name'],
                "category_code" => uniqid(""),
                "category_description" => $_POST['category_description']
            );
            $this->db->trans_start();
            $category_id = $this->crud->write('user_categories', $data);
            if (isset($_POST['permissions'])) {
                $permissions = $_POST['permissions'];
                foreach ($permissions as $permission)
                    $this->crud->write("category_permissions", array("category_id" => $category_id, "permission_id" => $permission));
            }
            $this->db->trans_complete();
            if ($this->db->trans_start())
                $this->session->set_flashdata('success', 'User Category registered successfully');
            redirect("client/" . $client_code, "refresh");
        }
    }

    function register_member()
    {
        $client = $this->crud->read("clients", array("client_id" => $_POST['client_id']));
        $client_code = $client[0]->client_code;
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('user_email', 'Member Email', 'required|valid_email|callback_check_email');
        $this->form_validation->set_rules('user_phone', 'Member Phone', 'required|numeric|callback_check_phone');
        $this->form_validation->set_rules('user_role', 'Member Role', 'required');
        $this->form_validation->set_rules('user_info', 'Member Info', 'required');
        if ($this->form_validation->run() == false) {
            $this->index($client[0]->client_code);
        } else {
            $data = array(
                "first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "client_id" => $_POST['client_id'],
                "user_code" => uniqid(""),
                "user_email" => $_POST['user_email'],
                "user_phone" => $_POST['user_phone'],
                "user_category" => $_POST['user_role'],
                "register_admin" => $this->session->userdata("logged_in")->admin_id,
                "user_info" => $_POST['user_info']
            );
            if ($this->crud->write('users', $data))
                $this->session->set_flashdata('success', 'User registered successfully');
            redirect("client/" . $client_code, "refresh");
        }
    }


    function check_email($email_address)
    {
        if (isset($_POST['user_email']))
            $records = $this->crud->read("users", array("user_email" => $email_address));
        if (count($records) != 0) {
            $this->form_validation->set_message('check_email', 'Email address provided already exists. Kindly change or edit the client details');
            return false;
        }
        return true;
    }

    function check_phone($phone_number)
    {
        if (isset($_POST['user_email']))
            $records = $this->crud->read("users", array("user_phone" => $phone_number));
        if (count($records) != 0) {
            $this->form_validation->set_message('check_phone', 'Phone number provided already exists. Kindly change or edit the client details');
            return false;
        }
        return true;
    }
}
