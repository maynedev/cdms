<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 5/26/16
 * Time: 12:31 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller
{

    protected $admin_id;
//
//    public function __construct()
//    {
////        $this->load->library('session');
////        var_dump($this->session->userdata('logged_in'));
//        $this->admin_id =$this->session->userdata("logged_in")->admin_id;
//    }

    public function index()
    {
        $var['page'] = array("Clients");
        $var['clients'] = $this->crud->read("clients", array("deleted" => false));
        $this->load->template('client', $var);
    }

    function add_new(){
        $var['page'] = array("Clients/Add New");
        $this->load->template('register_client', $var);
    }

    function register()
    {
        $this->form_validation->set_rules('client_name', 'Client Name', 'required');
        $this->form_validation->set_rules('client_email', 'Client Email', 'required|valid_email|callback_check_email');
        $this->form_validation->set_rules('client_phone', 'Client Phone', 'required|numeric|callback_check_phone');
        $this->form_validation->set_rules('client_contact_person', 'Contact Person', 'required');
        $this->form_validation->set_rules('client_partner', 'Contract Partner', 'required');
        $this->form_validation->set_rules('client_info', 'Client Info', 'required');
        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $data = array(
                "client_name" => $_POST['client_name'],
                "client_code" => uniqid(""),
                "client_email" => $_POST['client_email'],
                "client_phone" => $_POST['client_phone'],
                "client_contact_person" => $_POST['client_contact_person'],
                "client_partner" => $_POST['client_partner'],
                "register_admin" => $this->session->userdata("logged_in")->admin_id,
                "client_info" => $_POST['client_info']
            );

            if($this->crud->write('clients', $data)){
                $this->session->set_flashdata('success', 'Client registered successfully');
            }
            redirect("clients", "refresh");
        }
    }

    function check_email($email_address)
    {
        if (isset($_POST['user_email']))
            $records = $this->crud->read("users", array("user_email" => $email_address));
        else
            $records = $this->crud->read("clients", array("client_email" => $email_address));
        if (count($records) != 0) {
            $this->form_validation->set_message('check_email', 'Email address provided already exists. Kindly change or edit the client details');
            return false;
        }
        return true;
    }

    function check_phone($phone_number)
    {
        if (isset($_POST['user_email']))
            $records = $this->crud->read("users", array("user_phone" => $phone_number));
        else
            $records = $this->crud->read("clients", array("client_phone" => $phone_number));
        if (count($records) != 0) {
            $this->form_validation->set_message('check_phone', 'Phone number provided already exists. Kindly change or edit the client details');
            return false;
        }
        return true;
    }


}