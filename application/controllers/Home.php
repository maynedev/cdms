<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function index()
    {
        if (is_null($this->session->userdata('logged_in')))
            $this->login();
        else
            $this->dashboard();
    }

    function login()
    {
        if (!is_null($this->session->userdata('logged_in'))) {
            $this->dashboard();
            return;
        }
        $this->load->view('login_view');
    }

    function dashboard()
    {
        $var['page'] = array("Dashboard");
        $this->load->template('dashboard', $var);
    }

    public function validate()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|callback_validate_credentials');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run()) {
            redirect('home');
        } else {
            $this->index();
        }
    }

    public function validate_credentials($user_name)
    {
        $password = $_POST['password'];
        $login = $this->users->admin_login($user_name, $password);
        if (count($login) != 0) {
            $this->session->set_userdata('logged_in', (object)$login);
        } else {
            $this->form_validation->set_message('validate_credentials', 'Username and password provided do not match. Kindly check and try again');
            return false;
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('home');
    }
}
