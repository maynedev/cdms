<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 6/2/16
 * Time: 11:35 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller
{
    function index($category_code)
    {

//        $var['page'] = array("User Categories", $var['category']->category_name);
        $this->load->template('media', false);
    }

    function _remap($param)
    {
        if ($param == "delete")
            $this->remove_category();
        if ($param == "edit")
            $this->edit_category();
        else
            $this->index($param);
    }

    private function edit_category()
    {
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');
        $this->form_validation->set_rules('category_code', 'Category Code', 'required');
        $this->form_validation->set_rules('category_description', 'Category Description', 'required');
        if ($this->form_validation->run() == false) {
            $this->index($_POST['category_code']);
        } else {
            $this->db->trans_start();
            $this->crud->update("user_categories", array("category_code" => $_POST['category_code']),
                array("category_name" => $_POST['category_name'], "category_description" => $_POST['category_description']));
            if (isset($_POST['permissions'])) {
                foreach ($_POST['permissions'] as $permission)
                    $this->crud->write("category_permissions", array("category_id" => $_POST['category_id'], "permission_id" => $permission));
            }
            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                // Create user logs
                $this->session->set_flashdata('success', 'User Category details updated successfully');
            }else{
                $this->session->set_flashdata('error', 'User Category details update failed to complete');
            }
            redirect("category/" . $_POST['category_code'], 'refresh');
        }
    }

    private function remove_category(){
        $this->form_validation->set_rules('deletion_reason', 'Reason for removing', 'required');
        $this->form_validation->set_rules('category_code', 'Category Code', 'required');
        if ($this->form_validation->run() == false) {
            $this->index($_POST['category_code']);
        } else {
            $this->db->trans_start();
            $this->crud->update("user_categories", array("category_code" => $_POST['category_code']),array("deleted" => $_POST['action']));
            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                // Create user logs
                $this->session->set_flashdata('success', 'User Category details removed from list successfully. NOT COMPLETELY DELETED');
            }else{
                $this->session->set_flashdata('error', 'User Category details deletion failed to complete');
            }
            redirect("category/" . $_POST['category_code'], 'refresh');
        }
    }
}