<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 5/27/16
 * Time: 4:05 AM
 */
class Member extends CI_Controller
{
    function index($member_code)
    {
        $user =  $this->crud->read("users", array("user_code" => $member_code));
        if(count($user) == 0){
            redirect('nowhere');
        }
        $var['member'] = (object)$user[0];
        $var['page'] = array("Member profile", $var['member']->first_name);
        $this->load->template('member_profile', $var);
    }

    function _remap($param)
    {
        $this->index($param);
    }
}