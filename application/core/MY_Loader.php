<?php

/**
 * Created by PhpStorm.
 * User: mayneax
 * Date: 1/19/16
 * Time: 9:02 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Loader extends CI_Loader
{
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $CI =& get_instance();
        $logged_user = $CI->session->userdata('logged_in');
        if (!$logged_user) {
            redirect('home');
        } else {
            $vars["admin"] = $CI->session->userdata('logged_in');
            if ($return) {
                $content = $this->view('header_view', $vars, $return);
                $content .= $this->view($template_name . "_view", $vars, $return);
                $content .= $this->view('footer_view', $vars, $return);
                return $content;
            } else {
                $this->view('header_view', $vars);
                $this->view($template_name . "_view", $vars);
                $this->view('footer_view', $vars);
            }
        }
    }
}