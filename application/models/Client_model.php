<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 6/2/16
 * Time: 6:57 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model
{

    function get_category($category_id)
    {
        return $this->db->get_where("tbl_user_categories", array("user_category_id" => $category_id))->row();
    }

    function category_users($category_id)
    {
        return $this->db->get_where("tbl_users", array("deleted" => false, "user_category" => $category_id))->result();
    }

    function category_permissions($category_id)
    {
        return $this->db->select("a.*")
            ->from("tbl_permissions a")
            ->distinct()
            ->join("tbl_category_permissions b", "a.permission_id = b.permission_id")
            ->where(array("b.category_id" => $category_id, "a.deleted" => false, "b.deleted" => false))
            ->get()
            ->result();
    }

    function permission_category($category_id)
    {
        switch ($category_id) {
            case 1:
                return "System access";
            default:
                return "System unknown";
        }
    }

    function category_has_perm($permission_id, $category_id)
    {
        return count($this->db->get_where("tbl_category_permissions",
            array("deleted" => false, "category_id" => $category_id, "permission_id" => $permission_id))->row()) > 0;
    }

}