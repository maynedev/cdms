<?php

/**
 * Created by PhpStorm.
 * User: mayne
 * Date: 5/26/16
 * Time: 7:14 AM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function write($table, $data)
    {
        $this->db->insert("tbl_" . $table, $data);
        return $this->db->insert_id();
    }

    function read($table, $where)
    {
        return $this->db->get_where("tbl_" . $table, $where)
            ->result();
    }

    function update($table, $filter, $data)
    {
        return $this->db->where($filter)
            ->update('tbl_' . $table, $data);
    }

    function delete($table, $filter)
    {
        return $this->db->where($filter)
            ->update('tbl_' . $table, array("deleted" => true));
    }
}