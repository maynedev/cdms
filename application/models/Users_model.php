<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{

    protected $admin_id;

    function __construct()
    {
        parent::__construct();
        $this->admin_id = $this->session->userdata('logged_in');
    }

    /*
     * ADMIN FUNCTIONS
     */
    function admin_login($username, $password)
    {
        return $this->db->get_where("tbl_admin", array("admin_email" => $username, "admin_password" => md5($password)))->row();
    }

    function admin($admin_id)
    {
        return $this->db->get_where("tbl_admin", array("admin_id" => $admin_id))->row();
    }

    function get_status($status_id)
    {
        switch ($status_id) {
            case 1:
                return '<span class="label label-success">Active</span>';
            default:
                return '<span class="label label-danger">Inactive</span>';
        }
    }

}