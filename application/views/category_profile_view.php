<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <div class="col-sm-2 col-md-2">
                    <div style="padding:10px 0 10px 20px">
                        <img src="<?= base_url('assets/images/users/avatar-10.jpg') ?>" class="profile-pic">
                    </div>
                </div>
                <div class="col-sm-8 col-md-6">
                    <h3><?= $category->category_name ?></h3>
                    <p><?= $category->category_description ?></p>
                </div>
                <div class="col-sm-2 col-md-4">
                    <span class="pull-right">
                    <button class="btn btn-primary waves-effect waves-light m-t-10"
                            data-toggle="modal"
                            data-target="#new-category">
                        Edit
                    </button>
                    <button class="btn btn-danger waves-effect waves-light m-t-10"
                            data-toggle="modal"
                            data-target="#delete-category">
                        <?= $category->deleted ? 'Restore' : 'Remove' ?>
                    </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-md-12 col-lg-10">
                <h4 class="m-t-0 header-title"><b> Registered Users</b></h4>
            </div>
            <div class="col-md-12 col-lg-2">
                <button class="btn btn-xs btn-primary pull-right waves-effect waves-light m-t-10"
                        data-toggle="modal"
                        data-target="#con-close-modal">
                    New Member
                </button>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email Address</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Role</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($members as $member) { ?>
                        <tr>
                            <td><?= $member->user_id ?></td>
                            <td>
                                <a href="<?=site_url('user/'.$member->user_code)?>"> <?= $member->first_name ?></a>
                            </td>
                            <td>
                                <a href="<?=site_url('user/'.$member->user_code)?>"> <?= $member->last_name ?></a>
                            </td>
                            <td><?= $member->user_email ?></td>
                            <td><?= $member->user_phone ?></td>
                            <td><?= $this->users->get_status($member->user_status) ?></td>
                            <td><?= $this->client->get_category($member->user_category)->category_name ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b> Allowed Permissions</b></h4>
            <div class="col-md-12" style="margin-top: 15px">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($permissions as $permission) { ?>
                        <tr>
                            <td><?= $permission->permission_id ?></td>
                            <td>
                                <?= $permission->permission_name ?>
                            </td>
                            <td><?= $this->client->permission_category($permission->permission_category) ?></td>
                            <td><?= $permission->permission_description ?></td>
                            <td>
                                <button class="btn btn-xs btn-danger">Remove</button>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?= site_url('client/register_member') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Register New Member</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">First Name</label>
                                <input type="text" name="first_name" value="<?= set_value('first_name') ?>"
                                       class="form-control" id="field-1"
                                       placeholder="Test" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Last Name</label>
                                <input type="text" class="form-control" required name="last_name"
                                       value="<?= set_value('last_name') ?>"
                                       id="field-2" placeholder="Foo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Email Address</label>
                                <input type="email" class="form-control" required name="user_email"
                                       value="<?= set_value('user_email') ?>"
                                       id="field-3" placeholder="foo@example.com">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-4" class="control-label">Phone Number</label>
                                <input type="text" class="form-control" required name="user_phone"
                                       value="<?= set_value('user_phone') ?>"
                                       id="field-4" placeholder="+254flow">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Member Info</label>
                            <textarea name="user_info" class="form-control autogrow" id="field-7"
                                      placeholder="Write something about the client"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"><?= set_value('user_info') ?></textarea>
                            </div>
                        </div>
                    </div>
                    <input value="<?= $category->user_category_id ?>" name="user_role" type="hidden">
                    <input value="<?= $category->client_id ?>" name="client_id" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="delete-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?= site_url('category/delete') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><?= $category->deleted ? 'Restore' : 'Remove' ?> User Category</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Reason</label>
                            <textarea name="deletion_reason" class="form-control autogrow" id="field-7"
                                      placeholder="Kindly provide a reason why you want to perform this action"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <input value="<?= $category->category_code ?>" name="category_code" type="hidden">
                    <input value="<?= $category->deleted ? '0' : '1' ?>" name="action" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light"><?= $category->deleted ? 'Restore' : 'Remove' ?></button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
<div id="new-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?= site_url('category/edit') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit User Category</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Category Name</label>
                                <input type="text" class="form-control" required name="category_name"
                                       value="<?= $category->category_name ?>"
                                       id="field-3" placeholder="e.g Administrator, BDR">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-5" class="control-label">Add More Permissions</label>
                                <select name="permissions[]" id="field-5" class="select2 select2-multiple"
                                        multiple="multiple" multiple
                                        data-placeholder="Choose permissions to permissions...">
                                    <option disabled value="">Select Permissions</option>
                                    <?php foreach ($this->crud->read("permissions", array("deleted" => false)) as $permission) {
                                        if (!$this->client->category_has_perm($permission->permission_id, $category->user_category_id))
                                            printf('<option value="%d" %s>%s</option>', $permission->permission_id,
                                                in_array($permission->permssion_code, set_value('permissions[]')) ? "selected" : "", $permission->permission_name);
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Category Description</label>
                            <textarea name="category_description" class="form-control autogrow" id="field-7"
                                      placeholder="Write something about this category"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"><?= $category->category_description ?></textarea>
                            </div>
                        </div>
                    </div>
                    <input value="<?= $category->category_code ?>" name="category_code" type="hidden">
                    <input value="<?= $category->user_category_id ?>" name="category_id" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->