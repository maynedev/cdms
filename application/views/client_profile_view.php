<div class="row">
    <div class="col-sm-12">
        <div class="card-box"
             style="background: url('<?= base_url() ?>assets/images/gallery/2.jpg');padding:0 !important;">
            <div style="width: 100%; height: auto;margin-top: 100px;background: #FFFFFF">
                <div class="row">
                    <div class="col-sm-2 col-md-2">
                        <div style="padding:10px 0 10px 20px">
                            <img src="<?= base_url('assets/images/gallery/1.jpg') ?>" class="profile-pic">
                        </div>
                    </div>
                    <div class="col-sm-10 col-md-6">
                        <h3><?= $client->client_name ?></h3>
                        <h5 class="text-info"><?= $client->client_email ?></h5>
                        <p><?= $client->client_info ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="card-box" style="padding: 0 !important;">
            <ul class="list-group">
                <li class="list-group-item">
                    <strong>Company Name:</strong>
                    <span class="pull-right"><?= $client->client_name ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Email Address:</strong>
                    <span class="pull-right"> <?= $client->client_email ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Phone #:</strong>
                    <span class="pull-right"><?= $client->client_phone ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Sales Partner:</strong>
                    <span class="pull-right"> <?= $client->client_partner ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Contact Person:</strong>
                    <span class="pull-right"> <?= $client->client_contact_person ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Joined on:</strong>
                    <span class="pull-right"><?= $client->join_date ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Registered By:</strong>
                    <span class="pull-right"> <?= $client->register_admin ?></span>
                </li>
                <li class="list-group-item">
                    <strong>Code:</strong>
                    <span class="pull-right"><?= $client->client_code ?></span>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="card-box table-responsive">
            <h3>User Management</h3>
            <hr/>
            <div class="row">
                <div class="col-md-12 col-lg-10">
                    <h4 class="m-t-0 header-title"><b> User Categories</b></h4>
                </div>
                <div class="col-md-12 col-lg-2">
                    <button class="btn btn-xs btn-primary pull-right waves-effect waves-light m-t-10"
                            data-toggle="modal"
                            data-target="#new-category">
                        New User Category
                    </button>
                </div>
                <div class="col-md-12" style="margin-top: 15px">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>No. of Users</th>
                            <th>No. of Permissions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($user_categories as $category) { ?>
                            <tr>
                                <td><?= $category->user_category_id ?></td>
                                <td>
                                    <a href='<?= site_url("category/" . $category->category_code) ?>'><?= $category->category_name ?></a>
                                </td>
                                <td><?= $category->category_description ?></td>
                                <td><?= count($this->client->category_users($category->user_category_id)) ?></td>
                                <td><?= count($this->client->category_permissions($category->user_category_id)) ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="margin-top: 30px">
                <div class="col-md-12 col-lg-10">
                    <h4 class="m-t-0 header-title"><b> Registered Users</b></h4>
                </div>
                <div class="col-md-12 col-lg-2">
                    <button class="btn btn-xs btn-primary pull-right waves-effect waves-light m-t-10"
                            data-toggle="modal"
                            data-target="#con-close-modal">
                        New Member
                    </button>
                </div>
                <div class="col-md-12" style="margin-top: 10px">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Address</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($members as $member) { ?>
                            <tr>
                                <td><?= $member->user_id ?></td>
                                <td>
                                    <a href="<?=site_url('user/'.$member->user_code)?>"> <?= $member->first_name ?></a>
                                </td>
                                <td>
                                    <a href="<?=site_url('user/'.$member->user_code)?>"> <?= $member->last_name ?></a>
                                </td>
                                <td><?= $member->user_email ?></td>
                                <td><?= $member->user_phone ?></td>
                                <td><?= $this->users->get_status($member->user_status) ?></td>
                                <td><?= $this->client->get_category($member->user_category)->category_name ?></td>
                                <td>
                                    <a class="fa fa-edit btn-xs btn-primary"></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?= site_url('client/register_member') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Register New Member</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">First Name</label>
                                <input type="text" name="first_name" value="<?= set_value('first_name') ?>"
                                       class="form-control" id="field-1"
                                       placeholder="Test" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Last Name</label>
                                <input type="text" class="form-control" required name="last_name"
                                       value="<?= set_value('last_name') ?>"
                                       id="field-2" placeholder="Foo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Email Address</label>
                                <input type="email" class="form-control" required name="user_email"
                                       value="<?= set_value('user_email') ?>"
                                       id="field-3" placeholder="foo@example.com">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-4" class="control-label">Phone Number</label>
                                <input type="text" class="form-control" required name="user_phone"
                                       value="<?= set_value('user_phone') ?>"
                                       id="field-4" placeholder="+254flow">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-5" class="control-label">Role</label>
                                <select id="field-5" name="user_role" class="form-control select2">
                                    <option selected value="">Select User Role</option>
                                    <?php foreach ($this->crud->read("user_categories", array("deleted" => false, "client_id" => $client->client_id)) as $role) {
                                        printf('<option value="%d" %s>%s</option>', $role->user_category_id, $role->user_category_id == set_value('user_role') ? "selected" : "", $role->category_name);
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Member Info</label>
                            <textarea name="user_info" class="form-control autogrow" id="field-7"
                                      placeholder="Write something about the client"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"><?= set_value('user_info') ?></textarea>
                            </div>
                        </div>
                    </div>
                    <input value="<?= $client->client_id ?>" name="client_id" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
<div id="new-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?= site_url('client/register_category') ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Register New User Category</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Category Name</label>
                                <input type="text" class="form-control" required name="category_name"
                                       value="<?= set_value('category_name') ?>"
                                       id="field-3" placeholder="e.g Administrator, BDR">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-5" class="control-label">Permissions</label>
                                <select name="permissions[]" id="field-5" class="select2 select2-multiple"
                                        multiple="multiple" multiple
                                        data-placeholder="Choose allowed permissions...">
                                    <option disabled value="">Select Permissions</option>
                                    <?php foreach ($this->crud->read("permissions", array("deleted" => false)) as $permission) {
                                        printf('<option value="%d" %s>%s</option>', $permission->permission_id,
                                            in_array($permission->permssion_code, set_value('permissions[]')) ? "selected" : "", $permission->permission_name);
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Category Description</label>
                            <textarea name="category_description" class="form-control autogrow" id="field-7"
                                      placeholder="Write something about this category"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"><?= set_value('user_info') ?></textarea>
                            </div>
                        </div>
                    </div>
                    <input value="<?= $client->client_id ?>" name="client_id" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->