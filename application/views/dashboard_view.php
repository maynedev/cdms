<div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;">35%</span><canvas width="90" height="90"></canvas></div>
            <h3 class="text-success counter">2562</h3>
            <p class="text-muted text-nowrap">Total Sales today</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart circliful" data-dimension="90" data-text="75%" data-width="5" data-fontsize="14" data-percent="75" data-fgcolor="#3bafda" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;">75%</span><canvas width="90" height="90"></canvas></div>
            <h3 class="text-primary counter">5685</h3>
            <p class="text-muted text-nowrap">Daily visitors</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart circliful" data-dimension="90" data-text="58%" data-width="5" data-fontsize="14" data-percent="58" data-fgcolor="#f76397" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;">58%</span><canvas width="90" height="90"></canvas></div>
            <h3 class="text-pink">$ <span class="counter">12480</span></h3>
            <p class="text-muted text-nowrap">Total Earning</p>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="widget-simple-chart text-right card-box">
            <div class="circliful-chart circliful" data-dimension="90" data-text="49%" data-width="5" data-fontsize="14" data-percent="49" data-fgcolor="#98a6ad" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;">49%</span><canvas width="90" height="90"></canvas></div>
            <h3 class="text-inverse counter">62</h3>
            <p class="text-muted text-nowrap">Pending Orders</p>
        </div>
    </div>
</div>