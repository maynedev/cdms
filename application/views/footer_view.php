

<!-- Footer -->
<footer class="footer text-right">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                2016 © <?= $this->lang->line('system_name') ?>.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

</div> <!-- end container -->
</div>
<!-- End wrapper -->

<!-- jQuery  -->
<script src="<?=base_url()?>/assets/js/jquery.min.js"></script>
<script src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/assets/js/detect.js"></script>
<script src="<?=base_url()?>/assets/js/fastclick.js"></script>
<script src="<?=base_url()?>/assets/js/jquery.slimscroll.js"></script>
<script src="<?=base_url()?>/assets/js/jquery.blockUI.js"></script>
<script src="<?=base_url()?>/assets/js/waves.js"></script>
<script src="<?=base_url()?>/assets/js/wow.min.js"></script>
<script src="<?=base_url()?>/assets/js/jquery.nicescroll.js"></script>
<script src="<?=base_url()?>/assets/js/jquery.scrollTo.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/switchery/switchery.min.js"></script>

<!-- Datatables-->
<script src="<?=base_url()?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/jszip.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="<?=base_url()?>/assets/plugins/datatables/dataTables.scroller.min.js"></script>



<script src="<?=base_url()?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="<?=base_url()?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<!-- Datatable init js -->
<script src="<?=base_url()?>/assets/pages/datatables.init.js"></script>

<script src="<?=base_url()?>/assets/js/jquery.core.js"></script>
<script src="<?=base_url()?>/assets/js/jquery.app.js"></script>

<script type="text/javascript">
        TableManageButtons.init();

        jQuery(document).ready(function() {
            $(".select2").select2();
        });

</script>

</body>
</html>