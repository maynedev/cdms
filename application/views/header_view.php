<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="<?= base_url() ?>assets/images/index.png">

    <title><?= $this->lang->line('system_name') ?></title>

    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= base_url() ?>assets/js/modernizr.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-72255993-1', 'auto');
        ga('send', 'pageview');

    </script>


</head>

<body>


<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container">

            <!-- Logo container-->
            <div class="logo">
                <a href="<?= site_url() ?>" class="logo"><img style="height: 50px; width: auto"
                                                              src="<?= base_url('assets/images/index.png') ?>">
                    <span><?= $this->lang->line('system_name') ?></span> </a>
            </div>
            <!-- End Logo container-->

            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">
                    <li>
                        <form role="search" class="navbar-left app-search pull-left hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href="pages-blank.html"><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a href="javascript:void(0);" data-target="#" class="dropdown-toggle waves-effect waves-light"
                           data-toggle="dropdown" aria-expanded="true">
                            <i class="md md-notifications"></i> <span
                                class="badge badge-xs badge-pink">3</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg">
                            <li class="text-center notifi-title">Notification</li>
                            <li class="list-group nicescroll notification-list">
                                <!-- list item-->
                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-diamond noti-primary"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">A new order has been placed A new
                                                order has been placed</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <!-- list item-->
                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-cog noti-warning"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">New settings</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <!-- list item-->
                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-bell-o noti-success"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">Updates</h5>
                                            <p class="m-0">
                                                <small>There are <span class="text-primary">2</span> new
                                                    updates available
                                                </small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                            </li>

                            <li>
                                <a href="javascript:void(0);" class=" text-right">
                                    <small><b>See all notifications</b></small>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle waves-effect waves-light profile"
                           data-toggle="dropdown" aria-expanded="true"><img
                                src="<?= base_url() ?>assets/images/users/avatar-1.jpg" alt="user-img"
                                class="img-circle"> </a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                            <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                            <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                            <li><a href="<?= site_url('home/logout') ?>"><i class="ti-power-off m-r-5"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>
    <!-- End topbar -->


    <!-- Navbar Start -->
    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu">
                        <a href="<?= site_url() ?>"><i class="md md-dashboard"></i>Dashboard</a>
                    </li>
                    <li class="has-submenu">
                        <a href="<?=site_url('media')?>"><i class="md md-perm-media"></i>Media</a>
                        <ul class="submenu">
                            <li><a href="<?=site_url('media/video')?>"><i class="md md-video-collection"></i> Video</a></li>
                            <li><a href="<?=site_url('media/audio')?>"><i class="md md-audiotrack"></i> Audio</a></li>
                            <li><a href="<?=site_url('media/images')?>"><i class="md md-image"></i> Images</a></li>
                            <li><a href="<?=site_url('media/docs')?>"><i class="md md-receipt"></i> Documents</a></li>
                            <li><a href="<?=site_url('media/others')?>"><i class="md md-play-circle-outline"></i> Others</a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="javascript:void(0);"><i class="md md-invert-colors-on"></i>Program</a>
                        <ul class="submenu">
                            <li><a href="components-grid.html">Grid</a></li>
                            <li><a href="components-widgets.html">Widgets</a></li>
                            <li><a href="components-nestable-list.html">Nesteble</a></li>
                            <li><a href="components-range-sliders.html">Range Sliders </a></li>
                            <li><a href="components-sweet-alert.html">Sweet Alerts </a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="javascript:void(0);"><i class="md md-class"></i>Touch</a>
                        <ul class="submenu">
                            <li><a href="components-grid.html">Grid</a></li>
                            <li><a href="components-widgets.html">Widgets</a></li>
                            <li><a href="components-nestable-list.html">Nesteble</a></li>
                            <li><a href="components-range-sliders.html">Range Sliders </a></li>
                            <li><a href="components-sweet-alert.html">Sweet Alerts </a></li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="javascript:void(0);"><i class="md md-pages"></i>APS</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li class="active"><a href="pages-blank.html">Blank Page</a></li>
                                    <li><a href="pages-login.html">Login</a></li>
                                    <li><a href="pages-register.html">Register</a></li>
                                    <li><a href="pages-recoverpw.html">Recover Password</a></li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                                    <li><a href="pages-404.html">404 Error</a></li>
                                    <li><a href="pages-500.html">500 Error</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>


                    <li class="has-submenu">
                        <a href="javascript:void(0);"><i class="md md-folder-special"></i>Rapid</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="extras-timeline.html">Timeline</a></li>
                                    <li><a href="extras-invoice.html">Invoice</a></li>
                                    <li><a href="extras-calendar.html"> Calendar</a></li>
                                    <li><a href="extras-email-template.html">Email template</a></li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="extras-maintenance.html">Maintenance</a></li>
                                    <li><a href="extras-coming-soon.html">Coming-soon</a></li>
                                    <li><a href="extras-gallery.html"> Gallery</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="javascript:void(0);"><i class="md md-folder-special"></i>Reports</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="extras-timeline.html">Timeline</a></li>
                                    <li><a href="extras-invoice.html">Invoice</a></li>
                                    <li><a href="extras-calendar.html"> Calendar</a></li>
                                    <li><a href="extras-email-template.html">Email template</a></li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="extras-maintenance.html">Maintenance</a></li>
                                    <li><a href="extras-coming-soon.html">Coming-soon</a></li>
                                    <li><a href="extras-gallery.html"> Gallery</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                </ul>
                <!-- End navigation menu -->
            </div>
        </div>
    </div>
</header>
<!-- End Navigation Bar-->


<!-- =======================
     ===== START PAGE ======
     ======================= -->

<div class="wrapper">
    <div class="container">

        <!-- Page-Title -->
<!--        <div class="row">-->
<!--            <div class="col-sm-12">-->
<!--                <h4 class="page-title">Blank</h4>-->
<!--            </div>-->
<!--        </div>-->
        <!-- Page-Title -->


