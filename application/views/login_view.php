<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured sales force automation system.">
    <meta name="author" content="Maynedev.co.ke">

    <link rel="shortcut icon" href="<?= base_url('assets/images/index.png') ?>">

    <title>Ventas Mobile:: SFA </title>

    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/core.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/icons.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/components.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/pages.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/menu.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/responsive.css') ?>" rel="stylesheet" type="text/css">

    <script src="<?= base_url('assets/js/modernizr.min.js') ?>"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
<body>


<div class="wrapper-page">

    <div class="text-center">
        <a href="<?= site_url() ?>" class="logo logo-lg">
            <img height="100px" width="100px" src="<?= base_url('assets/images/index.png') ?>">
        </a>
    </div>

    <form class="form-horizontal m-t-20" action="<?= site_url('home/validate') ?>" method="post">
        <?php if (validation_errors() != "") { ?>
            <div class="alert alert-warning">
                <?= validation_errors() ?>
            </div>
        <?php } ?>
        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control" type="text" required="" value="<?=set_value('username')?>" name="username" placeholder="Username">
                <i class="md md-account-circle form-control-feedback l-h-34"></i>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control" type="password" value="<?=set_value('password')?>" required="" name="password" placeholder="Password">
                <i class="md md-vpn-key form-control-feedback l-h-34"></i>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary">
                    <input id="checkbox-signup" type="checkbox">
                    <label for="checkbox-signup">
                        Remember me
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group text-right m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Log In
                </button>
            </div>
        </div>

        <div class="form-group m-t-30">
            <div class="col-sm-7">
                <a href="<?= site_url('reset') ?>" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your
                    password?</a>
            </div>
        </div>
    </form>
</div>


<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/detect.js') ?>"></script>
<script src="<?= base_url('assets/js/fastclick.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.slimscroll.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.blockUI.js') ?>"></script>
<script src="<?= base_url('assets/js/waves.js') ?>"></script>
<script src="<?= base_url('assets/js/wow.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.nicescroll.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.scrollTo.min.js') ?>"></script>

<!-- Custom main Js -->
<script src="<?= base_url('assets/js/jquery.core.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.app.js') ?>"></script>

</body>
</html>