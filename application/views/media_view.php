<div class="row">
    <div class="col-sm-2">
        <div class="card-box" style="padding: 0 !important;">
            <ul class="list-group">
                <li class="list-group-item">
                    <strong>Video</strong>
                </li>
                <li class="list-group-item">
                    <strong>Audio</strong>
                </li>
                <li class="list-group-item active">
                    <strong>Images</strong>
                </li>
                <li class="list-group-item">
                    <strong>Documents</strong>
                </li>
                <li class="list-group-item">
                    <strong>Others</strong>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-10">
        <div class="card-box table-responsive">
            <div class="col-md-12 col-lg-10">
                <h4 class="m-t-0 header-title"><b>Created Media</b></h4>
            </div>
            <div class="col-md-12 col-lg-2">
                <button class="btn btn-primary pull-right waves-effect waves-light m-t-10" data-toggle="modal"
                        data-target="#con-close-modal">
                    New Media
                </button>
            </div>
            <table id="datatable-buttons" class="table table-striped">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Company Name</th>
                    <th>Email Address</th>
                    <th>Phone</th>
                    <th>Partner</th>
                    <th>Start date</th>
                    <th>Register Admin</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>


<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div><!-- /.modal -->