<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <form method="post" action="<?= site_url('clients/register') ?>">
                <div class="modal-header">
                    <h4 class="modal-title">Register New Client</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Name</label>
                                <input type="text" name="client_name" value="<?= set_value('client_name') ?>" class="form-control" id="field-1"
                                       placeholder="Ventas Mobile" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Partner</label>
                                <input type="text" class="form-control" required name="client_partner" value="<?= set_value('client_partner') ?>"
                                       id="field-2" placeholder="Partner One">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Email Address</label>
                                <input type="email" class="form-control" required name="client_email" value="<?= set_value('client_email') ?>"
                                       id="field-3" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-4" class="control-label">Phone Number</label>
                                <input type="text" class="form-control" required name="client_phone" value="<?= set_value('client_phone') ?>"
                                       id="field-4" placeholder="+254flow">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-5" class="control-label">Contact Person</label>
                                <input type="text" class="form-control" required name="client_contact_person"
                                       value="<?= set_value('client_contact_person') ?>" id="field-5" placeholder="John Doe">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Client Info</label>
                            <textarea name="client_info" class="form-control autogrow" id="field-7" placeholder="Write something about the client"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"><?= set_value('client_info') ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>