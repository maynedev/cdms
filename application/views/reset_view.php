<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured sales force automation system.">
    <meta name="author" content="Maynedev.co.ke">

    <link rel="shortcut icon" href="<?= base_url('assets/images/index.png') ?>">

    <title>Ventas Mobile :: SFA </title>

    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/core.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/icons.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/components.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/pages.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/menu.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/responsive.css') ?>" rel="stylesheet" type="text/css">

    <script src="<?= base_url('assets/js/modernizr.min.js') ?>"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
<body>

<body>


<div class="wrapper-page">

    <div class="text-center">
        <a href="<?= site_url() ?>" class="logo logo-lg">
            <img height="100px" width="160px" src="<?= base_url('assets/images/index.png') ?>">
        </a>
    </div>

    <form class="form-horizontal m-t-20" action="<?= site_url('reset/process') ?>">
        <div class="" style="padding-bottom: 20px">
           <b> Enter your email and instructions will be sent to you on how to reset!</b>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control" type="text" required="" name="email" placeholder="Email address">
                <i class="md md-account-circle form-control-feedback l-h-34"></i>
            </div>
        </div>


        <div class="form-group text-right m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Reset password
                </button>
            </div>
        </div>

        <div class="form-group m-t-30">
            <div class="col-sm-7">
                <a href="<?= site_url() ?>" class="text-muted"><i class="fa fa-lock m-r-5"></i> Got your password ?</a>
            </div>
        </div>
    </form>

</div>


<script>
    var resizefunc = [];
</script>


<!-- Main  -->
<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/detect.js') ?>"></script>
<script src="<?= base_url('assets/js/fastclick.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.slimscroll.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.blockUI.js') ?>"></script>
<script src="<?= base_url('assets/js/waves.js') ?>"></script>
<script src="<?= base_url('assets/js/wow.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.nicescroll.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.scrollTo.min.js') ?>"></script>

<!-- Custom main Js -->
<script src="<?= base_url('assets/js/jquery.core.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.app.js') ?>"></script>

</body>
</html>